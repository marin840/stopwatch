package com.example.threadsapp

import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.min

class MainActivity : AppCompatActivity() {

    var activityNumberOfMilliseconds = 0L
    var broji = false
    var doInBackGround: AsyncTask<Int, String, Unit>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startButton.setOnClickListener{
            if(!broji) {
                broji = true
                doInBackGround = DoInBackground()
                doInBackGround?.execute()
            }
            else{
                doInBackGround?.cancel(false)
                broji = false
                restartButton.setEnabled(true)
                startButton.text = "RESUME"
            }
        }

        restartButton.setOnClickListener{
            if(activityNumberOfMilliseconds!= 0L) {
                activityNumberOfMilliseconds = 0L
                timeTextView.text = "00:00:00"
                startButton.text = "START"
            }
        }
    }

    override fun onDestroy() {

        doInBackGround?.cancel(true)
        super.onDestroy()

    }

    inner class DoInBackground: AsyncTask<Int, String, Unit>() {

        override fun onCancelled() {
            super.onCancelled()
            activityNumberOfMilliseconds = milliseconds
        }

        var startTime = SystemClock.currentThreadTimeMillis()
        var milliseconds = 0L
        var seconds = 0L
        var minutes = 0L
        var ostatakMilli = 0L

        override fun onProgressUpdate(vararg values: String?) {
            super.onProgressUpdate(values[0])
            timeTextView.text = values[0]
        }

        override fun onPostExecute(result: Unit?) {
            super.onPostExecute(result)

        }

        override fun doInBackground(vararg params: Int?) {

            startTime = System.currentTimeMillis()
            startTime -= activityNumberOfMilliseconds

            while(!isCancelled){

                milliseconds = System.currentTimeMillis() - startTime
                ostatakMilli =milliseconds%1000/10
                seconds = milliseconds/1000
                minutes = seconds/60

                this.publishProgress(String.format("%02d:%02d:%02d", minutes, seconds, ostatakMilli))
                Thread.sleep(30)
            }

        }

        override fun onPreExecute() {
            super.onPreExecute()
            startButton.text = "PAUSE"
            restartButton.setEnabled(false)
        }

    }
}
